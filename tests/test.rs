use solana_erc20_sample::{
    Mint, TokenInstruction, Wallet, HEADER_SIZE, MINT_SIZE, U128, WALLET_SIZE,
};
use solana_program::{
    instruction::{AccountMeta, Instruction},
    pubkey::Pubkey,
    system_instruction::create_account,
};
use solana_program_test::{processor, BanksClient, ProgramTest};

use solana_program_test::tokio;
use solana_sdk::{
    signature::{Keypair, Signer},
    transaction::Transaction,
};
use speedy::Writable;

async fn read_mint(client: &mut BanksClient, pubkey: Pubkey) -> Mint {
    let mut mint = client.get_account(pubkey).await.unwrap().unwrap();
    *Mint::from_account_buf_mut(&mut mint.data).unwrap()
}

async fn read_wallet(client: &mut BanksClient, pubkey: Pubkey) -> Wallet {
    let mut wallet = client.get_account(pubkey).await.unwrap().unwrap();
    *Wallet::from_account_buf_mut(&mut wallet.data).unwrap()
}

async fn create_mint(
    client: &mut BanksClient,
    program_id: Pubkey,
    payer: &Keypair,
) -> (Pubkey, Keypair) {
    let recent_blockhash = client.get_recent_blockhash().await.unwrap();
    let mint_authority = Keypair::new();
    let mint_key = Keypair::new();

    let create_mint_transaction = Transaction::new_signed_with_payer(
        &[
            create_account(
                &payer.pubkey(),
                &mint_key.pubkey(),
                1_000_000_000,
                (MINT_SIZE + HEADER_SIZE) as u64,
                &program_id,
            ),
            Instruction {
                program_id,
                accounts: vec![
                    AccountMeta::new(mint_key.pubkey(), false),
                    AccountMeta::new_readonly(mint_authority.pubkey(), true),
                ],
                data: TokenInstruction::CreateMint {
                    decimals: 6,
                    name: (b"test token" as &[u8]).into(),
                    tag: (b"TTKN" as &[u8]).into(),
                }
                .write_to_vec()
                .expect("infallible"),
            },
        ],
        Some(&payer.pubkey()),
        &[&payer, &mint_key, &mint_authority],
        recent_blockhash,
    );

    client
        .process_transaction(create_mint_transaction)
        .await
        .unwrap();

    (mint_key.pubkey(), mint_authority)
}

async fn create_wallet(
    client: &mut BanksClient,
    program_id: Pubkey,
    payer: &Keypair,
    mint: Pubkey,
) -> (Pubkey, Keypair) {
    let recent_blockhash = client.get_recent_blockhash().await.unwrap();
    let wallet_authority = Keypair::new();
    let wallet_key = Keypair::new();

    let create_wallet_instruction = Transaction::new_signed_with_payer(
        &[
            create_account(
                &payer.pubkey(),
                &wallet_key.pubkey(),
                1_000_000_000,
                (WALLET_SIZE + HEADER_SIZE) as u64,
                &program_id,
            ),
            Instruction {
                program_id,
                accounts: vec![
                    AccountMeta::new_readonly(mint, false),
                    AccountMeta::new(wallet_key.pubkey(), false),
                    AccountMeta::new_readonly(wallet_authority.pubkey(), true),
                ],
                data: TokenInstruction::CreateWallet
                    .write_to_vec()
                    .expect("infallible"),
            },
        ],
        Some(&payer.pubkey()),
        &[&payer, &wallet_key, &wallet_authority],
        recent_blockhash,
    );

    client
        .process_transaction(create_wallet_instruction)
        .await
        .unwrap();

    (wallet_key.pubkey(), wallet_authority)
}

async fn mint_tokens(
    client: &mut BanksClient,
    program_id: Pubkey,
    payer: &Keypair,
    mint: Pubkey,
    mint_authority: &Keypair,
    wallet: Pubkey,
    amount: u128,
) {
    let recent_blockhash = client.get_recent_blockhash().await.unwrap();

    let trx = Transaction::new_signed_with_payer(
        &[Instruction {
            program_id,
            accounts: vec![
                AccountMeta::new(mint, false),
                AccountMeta::new_readonly(mint_authority.pubkey(), true),
                AccountMeta::new(wallet, false),
            ],
            data: TokenInstruction::MintTokens {
                amount: U128::new(amount),
            }
            .write_to_vec()
            .expect("infallible"),
        }],
        Some(&payer.pubkey()),
        &[payer, mint_authority],
        recent_blockhash,
    );

    client.process_transaction(trx).await.unwrap();
}

async fn burn_tokens(
    client: &mut BanksClient,
    program_id: Pubkey,
    payer: &Keypair,
    mint: Pubkey,
    mint_authority: &Keypair,
    wallet: Pubkey,
    amount: u128,
) {
    let recent_blockhash = client.get_recent_blockhash().await.unwrap();

    let trx = Transaction::new_signed_with_payer(
        &[Instruction {
            program_id,
            accounts: vec![
                AccountMeta::new(mint, false),
                AccountMeta::new_readonly(mint_authority.pubkey(), true),
                AccountMeta::new(wallet, false),
            ],
            data: TokenInstruction::BurnTokens {
                amount: U128::new(amount),
            }
            .write_to_vec()
            .expect("infallible"),
        }],
        Some(&payer.pubkey()),
        &[payer, mint_authority],
        recent_blockhash,
    );

    client.process_transaction(trx).await.unwrap();
}

async fn transfer(
    client: &mut BanksClient,
    program_id: Pubkey,
    payer: &Keypair,
    from: Pubkey,
    from_authority: &Keypair,
    to: Pubkey,
    amount: u128,
) {
    let recent_blockhash = client.get_recent_blockhash().await.unwrap();

    let trx = Transaction::new_signed_with_payer(
        &[Instruction {
            program_id,
            accounts: vec![
                AccountMeta::new(from, false),
                AccountMeta::new_readonly(from_authority.pubkey(), true),
                AccountMeta::new(to, false),
            ],
            data: TokenInstruction::TransferTokens {
                amount: U128::new(amount),
            }
            .write_to_vec()
            .expect("infallible"),
        }],
        Some(&payer.pubkey()),
        &[payer, from_authority],
        recent_blockhash,
    );

    client.process_transaction(trx).await.unwrap();
}

#[tokio::test]
async fn test() {
    let program_id = Pubkey::new_unique();

    let (mut client, payer, _) = ProgramTest::new(
        "erc20_sample",
        program_id,
        processor!(solana_erc20_sample::main),
    )
    .start()
    .await;

    let (mint_pk, mint_authority) = create_mint(&mut client, program_id, &payer).await;

    let mint = read_mint(&mut client, mint_pk).await;

    dbg!(mint.decimals);
    dbg!(mint.name());
    dbg!(mint.tag());

    assert_eq!(mint.decimals, 6);
    assert_eq!(mint.name(), "test token");
    assert_eq!(mint.tag(), "TTKN");

    let (wallet1, wallet1_authority) =
        create_wallet(&mut client, program_id, &payer, mint_pk).await;
    let (wallet2, wallet2_authority) =
        create_wallet(&mut client, program_id, &payer, mint_pk).await;

    mint_tokens(
        &mut client,
        program_id,
        &payer,
        mint_pk,
        &mint_authority,
        wallet1,
        1_000_000_000_000,
    )
    .await;

    transfer(
        &mut client,
        program_id,
        &payer,
        wallet1,
        &wallet1_authority,
        wallet2,
        5_000_000_000,
    )
    .await;

    let wallet1_data = read_wallet(&mut client, wallet1).await;
    let wallet2_data = read_wallet(&mut client, wallet2).await;
    let mint_data = read_mint(&mut client, mint_pk).await;

    assert_eq!(wallet1_data.authority, wallet1_authority.pubkey());
    assert_eq!(wallet2_data.authority, wallet2_authority.pubkey());
    assert_eq!(wallet1_data.mint, mint_pk);
    assert_eq!(wallet2_data.mint, mint_pk);
    let supply = mint_data.supply;
    assert_eq!(supply, 1_000_000_000_000);

    burn_tokens(
        &mut client,
        program_id,
        &payer,
        mint_pk,
        &mint_authority,
        wallet1,
        5_000_000_000,
    )
    .await;
    let mint_data = read_mint(&mut client, mint_pk).await;
    let supply = mint_data.supply;
    assert_eq!(supply, 1_000_000_000_000 - 5_000_000_000);
}
