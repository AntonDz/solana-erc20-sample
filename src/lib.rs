use std::mem::{align_of, size_of};
use std::{u128, usize};

#[macro_use]
extern crate solana_program;

#[macro_use]
extern crate static_assertions;

#[macro_use]
extern crate thiserror;

use solana_program::{
    account_info::AccountInfo, entrypoint::ProgramResult, log::sol_log,
    program_error::ProgramError, pubkey::Pubkey,
};
use speedy::{Context, Readable, Reader, Writable, Writer};

pub const HEADER_SIZE: usize = size_of::<Header>();
pub const MINT_SIZE: usize = size_of::<Mint>();
pub const WALLET_SIZE: usize = size_of::<Wallet>();

unsafe fn from_bytes_mut<A>(src: &mut [u8]) -> &mut A {
    assert!(src.len() == size_of::<A>());
    assert!(src.as_ptr() as usize % align_of::<A>() == 0);

    &mut *src.as_mut_ptr().cast()
}

#[repr(u32)]
#[derive(Error, Debug, PartialEq, Eq, Clone, Copy)]
pub enum Error {
    #[error("invalid account kind")]
    InvalidAccountKind,
    #[error("invalid account owner")]
    InvalidOwner,
    #[error("invalid instruction")]
    InvalidInstruction,
    #[error("account not empty")]
    AccountNotEmpty,
    #[error("invalid account size")]
    InvalidAccountSize,
    #[error("account not signed")]
    AccountNotSigned,
    #[error("account not writable")]
    AccountNotWritable,
    #[error("invalid utf-8")]
    InvalidUtf8,
    #[error("string too long")]
    InvalidStringLength,
    #[error("wrong authority")]
    WrongAuthority,
    #[error("wrong wallet")]
    WrongWallet,
    #[error("arithmetic overflow")]
    ArithmeticOverflow,
    #[error("mints do not match on wallets")]
    MintMismatch,
    #[error("insufficient funds")]
    InsufficientFunds,
}

impl From<Error> for ProgramError {
    fn from(error: Error) -> Self {
        ProgramError::Custom(error as u32)
    }
}

pub struct U128 {
    inner: u128,
}

impl U128 {
    pub fn new(value: u128) -> U128 {
        U128 { inner: value }
    }

    pub fn to_u128(&self) -> u128 {
        self.inner
    }
}

impl<C: Context> Writable<C> for U128 {
    fn write_to<T: ?Sized + Writer<C>>(&self, writer: &mut T) -> Result<(), C::Error> {
        let buf = self.inner.to_le_bytes();

        writer.write_bytes(&buf)
    }
}

impl<'a, C: Context> Readable<'a, C> for U128 {
    fn read_from<R: Reader<'a, C>>(reader: &mut R) -> Result<Self, C::Error> {
        let mut buf = [0u8; 16];
        reader.read_bytes(&mut buf)?;
        Ok(Self {
            inner: u128::from_le_bytes(buf),
        })
    }
}

#[derive(Readable, Writable)]
pub enum TokenInstruction {
    /// Initialize a new mint.
    ///
    /// Accounts:
    /// 0. [write] mint account
    /// 1. [sign] mint authority
    CreateMint {
        decimals: u8,
        name: Vec<u8>,
        tag: Vec<u8>,
    },
    /// Initialize a new wallet.
    ///
    /// Accounts:
    /// 0. [read] mint account
    /// 1. [write] wallet account
    /// 2. [sign] authority
    CreateWallet,
    /// Mint tokens to wallet.
    ///
    /// Accounts:
    /// 0. [write] mint account
    /// 1. [sign] mint authority
    /// 2. [write] target wallet
    MintTokens { amount: U128 },
    /// Burn tokens from wallet.
    ///
    /// Accounts:
    /// 0. [write] mint account
    /// 1. [sign] mint authority
    /// 2. [write] target wallet
    BurnTokens { amount: U128 },
    /// Transfer tokens from source to destination.
    ///
    /// Accounts:
    /// 0. [write] source wallet
    /// 1. [sign] source authority
    /// 2. [write] destination wallet
    TransferTokens { amount: U128 },
}

#[repr(u8)]
#[derive(PartialEq, Eq, Clone, Copy)]
pub enum AccountKind {
    Empty = 0,
    Mint = 1,
    Wallet = 2,
}

#[repr(packed)]
pub struct Header {
    pub kind: u8,
}

impl Header {
    pub fn from_account_buf_mut(buf: &mut [u8]) -> &mut Header {
        assert!(buf.len() >= HEADER_SIZE);

        let buf = &mut buf[..HEADER_SIZE];
        let header: &mut Self = unsafe { from_bytes_mut(buf) };

        assert!(header.is_valid());

        header
    }

    fn is_valid(&self) -> bool {
        matches!(self.kind, 0 | 1 | 2)
    }

    pub fn kind(&self) -> AccountKind {
        match self.kind {
            0 => AccountKind::Empty,
            1 => AccountKind::Mint,
            2 => AccountKind::Wallet,
            _ => unreachable!(),
        }
    }
}

#[repr(packed)]
#[derive(Clone, Copy)]
pub struct Mint {
    pub name: [u8; 32],
    pub tag: [u8; 4],
    pub decimals: u8,
    pub authority: Pubkey,
    pub supply: u128,
}

impl Mint {
    pub fn init_in_account_buf(buf: &mut [u8]) -> Result<&mut Mint, Error> {
        let header = Header::from_account_buf_mut(buf);

        if header.kind() != AccountKind::Empty {
            return Err(Error::AccountNotEmpty);
        }

        header.kind = AccountKind::Mint as u8;

        if buf.len() != HEADER_SIZE + MINT_SIZE {
            return Err(Error::InvalidAccountSize);
        }

        Ok(unsafe { from_bytes_mut(&mut buf[HEADER_SIZE..]) })
    }

    pub fn from_account_buf_mut(buf: &mut [u8]) -> Result<&mut Mint, Error> {
        let header = Header::from_account_buf_mut(buf);

        if header.kind() != AccountKind::Mint {
            return Err(Error::InvalidAccountKind);
        }

        assert!(buf.len() == HEADER_SIZE + MINT_SIZE);

        Ok(unsafe { from_bytes_mut(&mut buf[HEADER_SIZE..]) })
    }

    pub fn tag(&self) -> &str {
        let mut end = 0;
        while end < self.tag.len() && self.tag[end] != 0 {
            end += 1;
        }

        std::str::from_utf8(&self.tag).expect("mint tag is always valid utf-8")
    }

    pub fn name(&self) -> &str {
        let mut end = 0;
        while end < self.name.len() && self.name[end] != 0 {
            end += 1;
        }

        std::str::from_utf8(&self.name[..end]).expect("mint tag is always valid utf-8")
    }
}

#[repr(packed)]
#[derive(Clone, Copy)]
pub struct Wallet {
    pub mint: Pubkey,
    pub authority: Pubkey,
    pub balance: u128,
}

impl Wallet {
    pub fn init_in_account_buf(buf: &mut [u8]) -> Result<&mut Wallet, Error> {
        let header = Header::from_account_buf_mut(buf);

        if header.kind() != AccountKind::Empty {
            return Err(Error::AccountNotEmpty);
        }

        header.kind = AccountKind::Wallet as u8;

        if buf.len() != HEADER_SIZE + WALLET_SIZE {
            return Err(Error::InvalidAccountSize);
        }

        Ok(unsafe { from_bytes_mut(&mut buf[HEADER_SIZE..]) })
    }

    pub fn from_account_buf_mut(buf: &mut [u8]) -> Result<&mut Wallet, Error> {
        let header = Header::from_account_buf_mut(buf);

        if header.kind() != AccountKind::Wallet {
            return Err(Error::InvalidAccountKind);
        }

        assert!(buf.len() == HEADER_SIZE + WALLET_SIZE);

        Ok(unsafe { from_bytes_mut(&mut buf[HEADER_SIZE..]) })
    }
}

const_assert_eq!(HEADER_SIZE, 1);
const_assert_eq!(size_of::<Mint>(), 32 + 4 + 1 + 32 + 16);
const_assert_eq!(size_of::<Wallet>(), 32 + 32 + 16);

entrypoint!(main);

pub fn main(program_id: &Pubkey, accounts: &[AccountInfo], data: &[u8]) -> ProgramResult {
    if let Err(error) = process(program_id, accounts, data) {
        sol_log(&format!("Token program failed: {}", error));
        return Err(error.into());
    }

    Ok(())
}

fn process(program_id: &Pubkey, accounts: &[AccountInfo], data: &[u8]) -> Result<(), Error> {
    let instruction = TokenInstruction::read_from_buffer(data).map_err(|error| {
        sol_log(&format!("could not decode instruction: {}", error));

        Error::InvalidInstruction
    })?;

    match &instruction {
        TokenInstruction::CreateMint {
            decimals,
            name,
            tag,
        } => {
            if accounts.len() != 2 {
                return Err(Error::InvalidInstruction);
            }

            if std::str::from_utf8(name).is_err() || std::str::from_utf8(tag).is_err() {
                return Err(Error::InvalidUtf8);
            }

            let mint = &accounts[0];
            let authority = &accounts[1];

            if mint.owner != program_id {
                return Err(Error::InvalidOwner);
            }

            if !mint.is_writable {
                return Err(Error::AccountNotWritable);
            }

            if !authority.is_signer {
                return Err(Error::AccountNotSigned);
            }

            if name.len() > 32 || tag.len() > 4 {
                return Err(Error::InvalidStringLength);
            }

            let mut mint_data = mint.data.borrow_mut();
            let mint_data = Mint::init_in_account_buf(*mint_data)?;

            mint_data.decimals = *decimals;
            (&mut mint_data.name[..name.len()]).copy_from_slice(name);
            (&mut mint_data.tag[..tag.len()]).copy_from_slice(tag);
            mint_data.authority = *authority.key;
        }
        TokenInstruction::CreateWallet => {
            if accounts.len() != 3 {
                return Err(Error::InvalidInstruction);
            }

            let mint = &accounts[0];
            let wallet = &accounts[1];
            let authority = &accounts[2];

            if mint.owner != program_id || wallet.owner != program_id {
                return Err(Error::InvalidOwner);
            }

            if !authority.is_signer {
                return Err(Error::AccountNotSigned);
            }

            if !wallet.is_writable {
                return Err(Error::AccountNotWritable);
            }

            let mut mint_data = mint.data.borrow_mut();
            // validates that mint is valid
            Mint::from_account_buf_mut(*mint_data)?;

            let mut wallet_data = wallet.data.borrow_mut();
            let wallet_data = Wallet::init_in_account_buf(*wallet_data)?;

            wallet_data.authority = *authority.key;
            wallet_data.mint = *mint.key;
        }
        TokenInstruction::MintTokens { amount } | TokenInstruction::BurnTokens { amount } => {
            if accounts.len() != 3 {
                return Err(Error::InvalidInstruction);
            }

            let mint = &accounts[0];
            let authority = &accounts[1];
            let wallet = &accounts[2];

            if mint.owner != program_id || wallet.owner != program_id {
                return Err(Error::InvalidOwner);
            }

            if !authority.is_signer {
                return Err(Error::AccountNotSigned);
            }

            if !wallet.is_writable || !mint.is_writable {
                return Err(Error::AccountNotWritable);
            }

            let mut mint_data = mint.data.borrow_mut();
            let mint_data = Mint::from_account_buf_mut(*mint_data)?;

            let mut wallet_data = wallet.data.borrow_mut();
            let wallet_data = Wallet::from_account_buf_mut(*wallet_data)?;

            if mint_data.authority != *authority.key {
                return Err(Error::WrongAuthority);
            }

            if wallet_data.mint != *mint.key {
                return Err(Error::WrongWallet);
            }

            if matches!(instruction, TokenInstruction::BurnTokens { .. }) {
                wallet_data.balance = wallet_data
                    .balance
                    .checked_sub(amount.inner)
                    .ok_or(Error::ArithmeticOverflow)?;

                mint_data.supply = mint_data
                    .supply
                    .checked_sub(amount.inner)
                    .ok_or(Error::ArithmeticOverflow)?;
            } else {
                wallet_data.balance = wallet_data
                    .balance
                    .checked_add(amount.inner)
                    .ok_or(Error::ArithmeticOverflow)?;

                mint_data.supply = mint_data
                    .supply
                    .checked_add(amount.inner)
                    .ok_or(Error::ArithmeticOverflow)?;
            }
        }

        TokenInstruction::TransferTokens { amount } => {
            if accounts.len() != 3 {
                return Err(Error::InvalidInstruction);
            }

            let source = &accounts[0];
            let authority = &accounts[1];
            let destination = &accounts[2];

            if source.owner != program_id || destination.owner != program_id {
                return Err(Error::InvalidOwner);
            }

            if !authority.is_signer {
                return Err(Error::AccountNotSigned);
            }

            if !source.is_writable || !destination.is_writable {
                return Err(Error::AccountNotWritable);
            }

            let mut source_data = source.data.borrow_mut();
            let source_data = Wallet::from_account_buf_mut(*source_data)?;

            let mut destination_data = destination.data.borrow_mut();
            let destination_data = Wallet::from_account_buf_mut(*destination_data)?;

            if source_data.mint != destination_data.mint {
                return Err(Error::MintMismatch);
            }

            if source_data.authority != *authority.key {
                return Err(Error::WrongAuthority);
            }

            let amount = amount.inner;
            if source_data.balance < amount {
                return Err(Error::InsufficientFunds);
            }

            source_data.balance = source_data
                .balance
                .checked_sub(amount)
                .ok_or(Error::ArithmeticOverflow)?;
            destination_data.balance = destination_data
                .balance
                .checked_add(amount)
                .ok_or(Error::ArithmeticOverflow)?;
        }
    }

    Ok(())
}
